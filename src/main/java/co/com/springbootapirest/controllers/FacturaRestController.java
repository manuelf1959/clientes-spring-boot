package co.com.springbootapirest.controllers;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.FacturaDTO;
import co.com.springbootapirest.services.IClienteService;
import co.com.springbootapirest.services.IFacturaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
@Api("App Facturas")
public class FacturaRestController {

    @Autowired
    private IFacturaService facturaService;

    @GetMapping("/facturas")
    public List<FacturaDTO> consultarFacturas(){

        return facturaService.findFacturasAll();
    }

    @GetMapping("/facturas/{id}")
    public ResponseEntity<FacturaDTO> consultarFacturaPorId(@PathVariable Long id){
       return facturaService.findFacturaForId(id);
    }

    @PostMapping("/facturas")
    public ResponseEntity<HashMap<String,Object>> crearFactura(@RequestBody FacturaDTO factura){
        return facturaService.facturaSave(factura);
    }

    @DeleteMapping("/facturas/{id}")
    public ResponseEntity<HashMap<String,Object>> eliminarFactura(@PathVariable Long id){

        return facturaService.facturaDelete(id);
    }
}
