package co.com.springbootapirest.services;


import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ProductoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IProductoService {

    public List<ProductoDTO> findAll();

    public Page<ResponseEntity> findAll(Pageable pageable);

    public ResponseEntity findForId(Long id);

    public ResponseEntity save(ProductoDTO producto);

    public ResponseEntity delete(Long id);

    public ResponseEntity findProductoByNombre(String term);
}
