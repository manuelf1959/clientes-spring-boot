package co.com.springbootapirest.services.impl;

import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.FacturaDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Factura;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import co.com.springbootapirest.dao.IClienteDao;
import co.com.springbootapirest.dao.IFacturaDao;
import co.com.springbootapirest.services.IClienteService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    private IClienteDao clienteDao;

    ObjectMapper mapper = new ObjectMapper();
    Cliente cliente;
    HashMap<String, Object> map = new HashMap<String, Object>();

    @Override
    @Transactional(readOnly = true)
    public List<ClienteDTO> findAll() {
        List<Cliente> clientes;
        List<ClienteDTO> clientesDTO= new ArrayList<ClienteDTO>();

        clientes = clienteDao.findAll();
        for (Cliente client:clientes){
            clientesDTO.add( ClienteDTO.builder()
                    .nombre(client.getNombre())
                    .apellido(client.getApellido())
                    .email(client.getEmail())
                    .createAt(client.getCreateAt())
                    .foto(client.getFoto())
                    .region(client.getRegion())
                    .build());
        }

        return clientesDTO;
    }


    @Override
    @Transactional(readOnly = true)
    public Page<ClienteDTO> findAll(Pageable pageable) {

        Page clinetesByPage = clienteDao.findAll(pageable);

        return clinetesByPage;
    }

    @Override
    @Transactional(readOnly = true)
    public ClienteDTO findForId(Long id) {



        ///////////////////////////////////////////////////////////////
        cliente = clienteDao.findById(id).orElse(null);

        return mapper.convertValue(cliente, ClienteDTO.class);
    }

    @Override
//    @Transactional
    public ClienteDTO save(ClienteDTO clienteDTO) {
        cliente = mapper.convertValue(clienteDTO, Cliente.class);
        cliente = clienteDao.save(cliente);
        ClienteDTO clientTemp = ClienteDTO.builder()
                .nombre(cliente.getNombre())
                .apellido(cliente.getApellido())
                .createAt(cliente.getCreateAt())
                .email(cliente.getEmail())
                .build();
        return clientTemp;

    }

    @Override
    @Transactional
    public void delete(Long id) {
        clienteDao.deleteById(id);
    }




}
