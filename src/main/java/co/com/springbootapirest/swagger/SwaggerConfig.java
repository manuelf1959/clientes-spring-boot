package co.com.springbootapirest.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {

        Docket dock = new Docket(DocumentationType.SWAGGER_2).apiInfo(metadata())
                .select()
                .apis(RequestHandlerSelectors.basePackage("co.com.springbootapirest.controllers"))
                .paths(PathSelectors.any())
                .build();
        return dock.useDefaultResponseMessages(false);
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder().title("SPRING BOOT API")
                .description("Servicios CRUD para clientes")
                .version("1.0")
                .contact("Contacto")
                .license("Licencia")
                .licenseUrl(" http://www.urllicencia.com")
                .termsOfServiceUrl("Terminos de servicio")
                .build();
    }
}
