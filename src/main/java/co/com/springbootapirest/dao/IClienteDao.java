package co.com.springbootapirest.dao;



import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface IClienteDao extends JpaRepository<Cliente, Long> {


}
