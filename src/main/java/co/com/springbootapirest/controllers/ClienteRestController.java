package co.com.springbootapirest.controllers;


import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import co.com.springbootapirest.services.IClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.spi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
@Api("Mi primera App")
public class ClienteRestController {

    @Autowired
    private IClienteService clienteService;

    private final int RESTROS_POR_PAGINA = 10;
    @GetMapping("/clientes")
    @ApiOperation("Consulta Todos los Clientes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ClienteDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = String.class, message = "No se Encontro Cliente"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error")

    })
    @PermitAll
    public List<ClienteDTO> consultarTodos(){

    return clienteService.findAll();

    }


    @PostMapping("/clientes/upload")
    @ApiOperation("Create new Foto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ClienteDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = ClienteDTO.class, message = "NO se Encontro Cliente"),
            @ApiResponse(code = 500, response = ClienteDTO.class, message = "Internal Server Error")

    })
    public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
        HashMap<String, Object> map = new HashMap<>();

        ClienteDTO cliente = clienteService.findForId(id);

        if (!archivo.isEmpty()){
            String nombreArchivo =  UUID.randomUUID().toString() +"_" +archivo.getOriginalFilename();
            Path rutaArchivo = Paths.get("upload").resolve(nombreArchivo).toAbsolutePath();
            try{
                Files.copy(archivo.getInputStream(), rutaArchivo);
            }catch (IOException e){

                e.printStackTrace();
                map.put("mensaje", "Subir la Imagen");
                map.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
                return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);

            }

            String nombrefotoAnterior = cliente.getFoto();

            if(nombrefotoAnterior != null && nombrefotoAnterior.length() > 0  ){
                Path rutaAnterior = Paths.get("upload").resolve(nombrefotoAnterior).toAbsolutePath();
                File archivoFotoAnterior = rutaAnterior.toFile();
                if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()){
                    archivoFotoAnterior.delete();
                }

            }

            cliente.setFoto(nombreArchivo);
            clienteService.save(cliente);

            map.put("Cliente", cliente);
            map.put("mensaje", "Se ha subido correctamente");


        }


        return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.CREATED);


    }



    @GetMapping("/clientes/page/{page}")
    @ApiOperation("Consulta Todos los Clientes por pagina")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ClienteDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = String.class, message = "NO se Encontro Cliente"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error")

    })
    public Page<ClienteDTO> consultarTodos(@PathVariable Integer page){
        PageRequest pageRequest = PageRequest.of(page, RESTROS_POR_PAGINA);
        return clienteService.findAll(pageRequest);

    }

    @GetMapping("/clientes/{id}")
    @ApiOperation("Consulta Clientes por su ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ClienteDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = String.class, message = "NO se Encontro Cliente"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error")

    })
    public ResponseEntity<?> consultarPorId(@PathVariable Long id){
        ClienteDTO cliente = null;
        HashMap<String, Object> map = new HashMap<>();

        try {
            cliente =  clienteService.findForId(id);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la consulta a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if(cliente == null){
            map.put("mensaje","El CLiente con el ID: ".concat(id.toString()).concat(" No existe en la BD "));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<ClienteDTO>(cliente, HttpStatus.OK);
    }

    @PostMapping ("/clientes")
    @ApiOperation("Crear  Clientes")
    @ApiResponses(value = {
            @ApiResponse(code = 201, response = ClienteDTO.class, message = "CREATE"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error")

    })
    public ResponseEntity<?> create(@RequestBody ClienteDTO cliente, @Valid BindingResult result){
        ClienteDTO clienteNew = null;
        //ClienteDTO clientetemp = new ClienteDTO(0,null,null,null,null);
        HashMap<String, Object> map = new HashMap<>();
        //Segunda Opcion para version menores a Java 8
        if (result.hasErrors()) {
            /*List<String> errors = new ArrayList<>();
            for (FieldError fileError: result.getFieldErrors()){
                errors.add("El campo '" + fileError.getField() + "' " + fileError.getDefaultMessage());
            }*/


            // Metodo para Java 8
            List<String> errors = result.getFieldErrors().stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage()).collect(Collectors.toList());
            map.put("errores", errors);
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.BAD_REQUEST);
        }
        try {
            clienteNew =  clienteService.save(cliente);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la insercion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        map.put("mensaje: ","Cliente Insertado Correctamente");
        map.put("Cliente: ",clienteNew.toString());

        return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.OK);
    }


    @PutMapping("/clientes/{id}")
    @ApiOperation("Consulta Todos los Clientes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ClienteDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = String.class, message = "NO se Encontro Cliente"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error"),

    })
    public ResponseEntity<?> update( @RequestBody ClienteDTO cliente,@Valid BindingResult result ,@PathVariable Long id){
        ClienteDTO temp = null;
        HashMap<String, Object> map = new HashMap<>();
        temp = clienteService.findForId(id);
        ClienteDTO clienteActualizado;
        if(result.hasErrors()){
            List<String> errores = result.getFieldErrors().stream().map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            map.put("errores", errores);
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.BAD_REQUEST);
        }

        if(temp == null){
            map.put("mensaje","El CLiente con el ID: ".concat(id.toString()).concat(" No existe en la BD "));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NOT_FOUND);
        }
        try{
            temp.setNombre(cliente.getNombre());
            temp.setApellido(cliente.getApellido());
            temp.setEmail(cliente.getEmail());
            clienteActualizado = clienteService.save(temp);

        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la Actualizacion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<ClienteDTO>(clienteActualizado, HttpStatus.CREATED);

    }

    @DeleteMapping("/clientes/{id}")
    @ApiOperation("Elimina clientes por ID")
    @ApiResponses(value = {
            @ApiResponse(code = 204, response = ClienteDTO.class, message = "NO CONTENT"),
            @ApiResponse(code = 404, response = String.class, message = "NO se Encontro Cliente"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error"),

    })
    public ResponseEntity<?> delete(@PathVariable Long id ){
        HashMap<String, Object> map = new HashMap<>();
        try{
            ClienteDTO cliente = clienteService.findForId(id);
            String nombrefotoAnterior = cliente.getFoto();

            if(nombrefotoAnterior != null && nombrefotoAnterior.length() > 0  ){
                Path rutaAnterior = Paths.get("upload").resolve(nombrefotoAnterior).toAbsolutePath();
                File archivoFotoAnterior = rutaAnterior.toFile();
                if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()){
                    archivoFotoAnterior.delete();
                }

            }
 clienteService.delete(id);
        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la Actualizacion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        map.put("mensaje", "Se ha eliminado el Cliente Correctamente");
        return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.NO_CONTENT);
    }





}
