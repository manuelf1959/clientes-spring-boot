package co.com.springbootapirest.services;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.FacturaDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IFacturaService {

    public List<FacturaDTO> findFacturasAll();
    public ResponseEntity findFacturaForId(Long id);
    public ResponseEntity facturaSave(FacturaDTO facturaDTO);
    public ResponseEntity facturaDelete(Long id);
}
