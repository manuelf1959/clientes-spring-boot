package co.com.springbootapirest.services.impl;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import co.com.springbootapirest.dao.IRegionDao;
import co.com.springbootapirest.services.IRegionService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
@Service
public class RegionServiceImpl implements IRegionService {

    @Autowired
    private IRegionDao regionDao;

    ObjectMapper mapper = new ObjectMapper();
    HashMap<String, Object> map = new HashMap<String, Object>();


    @Override
    @Transactional(readOnly = true)
    public List<RegionDTO> findRegiones() {
        List<Region> regiones = regionDao.findAllRegiones();
        List<RegionDTO> regionesDTO;
        regionesDTO = mapper.convertValue(regiones, new TypeReference<List<RegionDTO>>() {});


        return regionesDTO;
    }

}
