package co.com.springbootapirest.services;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;

import java.util.List;

public interface IRegionService {

    public List<RegionDTO> findRegiones();
}
