package co.com.springbootapirest.controllers;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ProductoDTO;
import co.com.springbootapirest.services.IProductoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
@Api("App Productos")
public class ProductoRestController {

    @Autowired
    private IProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoDTO> consultarTodosProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    @ApiOperation("Consultar Productos por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ProductoDTO.class, message = "OK"),
            @ApiResponse(code = 400, response = HashMap.class, message = "No Encontrado"),
            @ApiResponse(code = 500, response = HashMap.class, message = "Internal Server Error")

    })
    public ResponseEntity consultarProductosById(@PathVariable  Long id){
        return productoService.findForId(id);
    }
    @GetMapping("/productos/filtrar-Producto/{term}")
    @ApiOperation("Consultar Productos por Nombre")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ProductoDTO.class, message = "OK"),
            @ApiResponse(code = 400, response = HashMap.class, message = "No Encontrado"),
            @ApiResponse(code = 500, response = HashMap.class, message = "Internal Server Error")

    })
    public ResponseEntity consultarProductosByNombre(@PathVariable  String term){
        return productoService.findProductoByNombre(term);
    }
    @PostMapping("/productos")
    @ApiOperation("Crear Producto ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = ProductoDTO.class, message = "OK"),
            @ApiResponse(code = 500, response = HashMap.class, message = "Internal Server Error")

    })
    public ResponseEntity crearProducto(@RequestBody  ProductoDTO producto){
        return productoService.save(producto);
    }
    @DeleteMapping("/productos/{id}")
    @ApiOperation("Borrar Productos ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = HashMap.class, message = "NO_CONTENT"),
            @ApiResponse(code = 500, response = HashMap.class, message = "Internal Server Error")

    })
    public ResponseEntity borrarProducto(@PathVariable Long id){
        return productoService.delete(id);
    }
}
