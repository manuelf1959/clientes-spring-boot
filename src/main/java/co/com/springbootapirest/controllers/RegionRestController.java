package co.com.springbootapirest.controllers;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.services.IRegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
@Api("App Regiones")
public class RegionRestController {

    @Autowired
    private IRegionService regionService;

    @GetMapping("/regiones")
    @ApiOperation("Consulta Todos las Regiones")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = RegionDTO.class, message = "OK"),
            @ApiResponse(code = 404, response = String.class, message = "NO se Encontro Regiones"),
            @ApiResponse(code = 500, response = String.class, message = "Internal Server Error"),

    })
    public List<RegionDTO> consultarRegiones(){
        return regionService.findRegiones();
    }
}
