package co.com.springbootapirest.services.impl;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ProductoDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Producto;
import co.com.springbootapirest.dao.IProductoDao;
import co.com.springbootapirest.services.IProductoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Service
public class ProductoServiceImpl implements IProductoService {


    @Autowired
    private IProductoDao productoDao;
    private Producto producto;

    ObjectMapper mapper = new ObjectMapper();
    List<Producto> productos= new ArrayList<Producto>();
    List<ProductoDTO> productosDTO = new ArrayList<ProductoDTO>();
    ProductoDTO productoDTO = new ProductoDTO();

    @Override
    public List<ProductoDTO> findAll() {

        productos = productoDao.findAll();
        productosDTO = mapper.convertValue(productos, new TypeReference<List<ProductoDTO>>() {});

        return productosDTO;
    }

    @Override
    public Page<ResponseEntity> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public ResponseEntity findForId(Long id) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        try{

            Producto producto = new Producto();
            producto = productoDao.findById(id).orElse(null);
            if(producto == null){
                map.put("Parametro: ", id);
                map.put("Mensaje: ", "El Producto con Id: " + id + " No se encuentra en la DB");

                return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.NOT_FOUND);
            }
            productoDTO = mapper.convertValue(producto, ProductoDTO.class);
            return new ResponseEntity<>(productoDTO, HttpStatus.OK);
        }catch (DataAccessException e){
            map.put("Parametro: ", id);
            map.put("Mensaje: ", "Error en el Proceso " + e.getCause());
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @Override
    public ResponseEntity save(ProductoDTO producto) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        try{
            Producto productoNew = new Producto();
            productoNew = mapper.convertValue(producto, Producto.class);
            productos = productoDao.findByNombre(producto.getNombre());
            for(Producto pro: productos){
                if(pro.getNombre().equals(productoNew.getNombre())){
                    map.put("Mensaje: ", "Error al Crear Producto");
                    map.put("Error: ", "Ya existe un Producto con el mismo Nombre");
                    return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.CONFLICT);
                }
            }
            productoNew = productoDao.save(productoNew);
            map.put("Mensaje: ", "Crado Correctamente");
            map.put("Producto: ", productoNew.toString());
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.OK);
        }catch (DataAccessException e){
            map.put("Producto: ", producto);
            map.put("Mensaje: ", "Error en creacion " + e.getCause());
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @Override
    public ResponseEntity delete(Long id) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        try{
            productoDao.deleteById(id);
            map.put("Mensaje","Producto Borrado Exitosamente");
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.NO_CONTENT);
        }catch (DataAccessException e){
            map.put("Mensaje","Error al Borrar el Producto");
            map.put("Error", e.getCause());
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<?> findProductoByNombre(String term) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        try{
            productos = productoDao.findByNombre(term);
            productosDTO = mapper.convertValue(productos, new TypeReference<List<ProductoDTO>>(){});
//|           return new ResponseEntity<List<ProductoDTO>>(productosDTO, HttpStatus.OK);
        }catch (DataAccessException e){
            map.put("Parametro: ", term);
            map.put("Mensaje: ", "Error en el Proceso " + e.getCause());

            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }if(productos.isEmpty()){
            map.put("Parametro: ", term);
            map.put("Mensaje: ", "No se Encontraron Coincidencias");

            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.NOT_FOUND);
        }


        return new ResponseEntity<List<ProductoDTO>>(productosDTO, HttpStatus.OK);

    }
}
