package co.com.springbootapirest.co.com.springbootapirest.models.DTO;

import co.com.springbootapirest.co.com.springbootapirest.models.Producto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.io.Serializable;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ItemFacturaDTO implements Serializable {

    private Long id;
    private Integer cantidad;
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductoDTO producto;
    private String nombre;
}
