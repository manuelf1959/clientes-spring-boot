package co.com.springbootapirest.dao;

import co.com.springbootapirest.co.com.springbootapirest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {

//    public Usuario findByUserName(String userName);
//
//
//    public Usuario buscarPorId(String userName);
}
