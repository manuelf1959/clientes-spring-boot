package co.com.springbootapirest.co.com.springbootapirest.models.DTO;

import co.com.springbootapirest.co.com.springbootapirest.models.Factura;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClienteDTO implements Serializable{

    @NotEmpty
    private  long id;
    @NotEmpty
    private String nombre;
    @NotEmpty
    private String apellido;
    @NotEmpty
    private String email;
    @NotEmpty
    private Date createAt;
    @NotEmpty
    private String foto;
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    private Region region;
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    private List<FacturaDTO> facturas;


}
