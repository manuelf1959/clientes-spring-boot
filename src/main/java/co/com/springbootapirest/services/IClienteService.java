package co.com.springbootapirest.services;

import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.ClienteDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.FacturaDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.DTO.RegionDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Factura;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IClienteService {

    public List<ClienteDTO>  findAll();
    public Page<ClienteDTO> findAll(Pageable pageable);
    public ClienteDTO findForId(Long id);
    public ClienteDTO save(ClienteDTO cliente);
    public void delete (Long id);



}
