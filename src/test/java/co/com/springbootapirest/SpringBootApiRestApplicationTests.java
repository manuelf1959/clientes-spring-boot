package co.com.springbootapirest;

import co.com.springbootapirest.dao.IProductoDao;
import co.com.springbootapirest.services.impl.ProductoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootApiRestApplicationTests {

    @InjectMocks
    ProductoServiceImpl iProductoService;

    @Mock
    IProductoDao iProductoDao;

    @Test
    public void contextLoads() {
    }

    @Test
    public void findAlltest(){
        iProductoService.findAll();
        verify(iProductoDao).findAll();
    }
    Long num = new Long(1);

    @Test
    public void findByIdtest(){
        iProductoService.findForId(num);
        verify(iProductoDao).findById(num);
    }

}
