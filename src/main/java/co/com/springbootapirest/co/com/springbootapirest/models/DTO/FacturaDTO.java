package co.com.springbootapirest.co.com.springbootapirest.models.DTO;

import co.com.springbootapirest.co.com.springbootapirest.models.Cliente;
import co.com.springbootapirest.co.com.springbootapirest.models.ItemFactura;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FacturaDTO implements Serializable {

    private Long id;
    private String descripcion;
    private String observacion;
    private Date createAt;
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler","facturas"})
    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente cliente;
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ItemFacturaDTO> items;
}
