package co.com.springbootapirest.dao;

import co.com.springbootapirest.co.com.springbootapirest.models.Factura;
import co.com.springbootapirest.co.com.springbootapirest.models.Region;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IFacturaDao extends CrudRepository<Factura, Long> {

    @Query("from Factura")
    List<Factura> findAllFacturas();
}
