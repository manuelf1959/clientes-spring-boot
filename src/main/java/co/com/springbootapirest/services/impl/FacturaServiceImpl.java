package co.com.springbootapirest.services.impl;

import co.com.springbootapirest.co.com.springbootapirest.models.DTO.FacturaDTO;
import co.com.springbootapirest.co.com.springbootapirest.models.Factura;
import co.com.springbootapirest.dao.IFacturaDao;
import co.com.springbootapirest.services.IFacturaService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
@Service
public class FacturaServiceImpl implements IFacturaService {

    @Autowired
    private IFacturaDao facturaDao;

    ObjectMapper mapper = new ObjectMapper();
    HashMap<String, Object> map = new HashMap<String, Object>();


    @Override
    @Transactional(readOnly = true)
    public List<FacturaDTO> findFacturasAll() {
        List<Factura> facturas = facturaDao.findAllFacturas();
        List<FacturaDTO> facturaDTO =  mapper.convertValue(facturas, new TypeReference<List<FacturaDTO>>(){});
        return facturaDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<?> findFacturaForId(Long id) {

        try {
            Factura factura = facturaDao.findById(id).orElse(null);
            FacturaDTO facturaDTO = mapper.convertValue(factura,FacturaDTO.class);
            if (factura == null){
                map.put("mensaje", "Factura No existe");
                return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<FacturaDTO>(facturaDTO, HttpStatus.OK);

        }catch (DataAccessException e){
            map.put("mensaje, ", "error al consultar la Base de Datos ");
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @Override
    @Transactional
    public ResponseEntity facturaSave(FacturaDTO facturaDTO) {
        try {
            Factura factura = mapper.convertValue(facturaDTO, Factura.class);
            Factura facturaNew =  facturaDao.save(factura);
            facturaDTO = mapper.convertValue(facturaNew, FacturaDTO.class);
            map.put("mensaje", "Factura creada correctamente");
            map.put("Factura", facturaDTO.toString());
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.OK);

        }catch (DataAccessException e){
            map.put("mensaje", "Error al realizar la insercion a la base de datos");
            map.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
            return new ResponseEntity<HashMap<String,Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    @Transactional
    public ResponseEntity facturaDelete(Long id) {
        try{
            facturaDao.deleteById(id);
            map.put("Mensaje", "La Factura Se Elimino Correctamente");
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.NO_CONTENT);
        }catch (Exception e ){
            map.put("mensaje", "Error al eliminar la Factura");
            return new ResponseEntity<HashMap<String, Object>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }
}
