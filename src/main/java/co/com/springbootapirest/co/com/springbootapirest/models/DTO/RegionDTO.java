package co.com.springbootapirest.co.com.springbootapirest.models.DTO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.ws.rs.core.Response;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegionDTO implements Serializable {

    private long id;
    private String nombre;




}
