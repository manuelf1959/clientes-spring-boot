package co.com.springbootapirest.co.com.springbootapirest.models.DTO;

import co.com.springbootapirest.co.com.springbootapirest.models.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO implements Serializable {

    private Long id;
    private String userName;
    private String password;
    private Boolean enabled;
    private List<Role> roles;
}
